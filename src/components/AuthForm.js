import { useState } from "react";
import { IMaskMixin } from "react-imask";
import toast from "react-hot-toast";

import classes from "./AuthForm.module.scss";
import usernameIcon from "../assets/akar-icons_person.svg";
import { ReactComponent as OtpCodeIcon } from "../assets/carbon_password.svg";
import { ReactComponent as PhoneIcon } from "../assets/phone.svg";

import { RecaptchaVerifier, signInWithPhoneNumber } from "firebase/auth";
import { auth } from "../firebase";

/**
 * @param mask string
 * @example
 * <MaskedInput
 *  mask="996(000)000-000"
 * />
 * <MaskedInput
 *  mask="996-000-000-000"
 * />
 * <MaskedInput
 *  mask="996000000000"
 * />
 * и т.д.
 */
const MaskedInput = IMaskMixin(({ inputRef, ...props }) => (
    <input ref={inputRef} {...props} />
));

/**
 * @param onSuccessAuth () => void
 * @example
 * 
 * const onSuccessAuth = useCallback(
        (name) => {
            setIsAuth(true);
            toast.success(`Вы успешно прошли авторизацию`);
            setUser(name);
        },
        [setIsAuth, setUser]
    );
 *  */

function AuthForm({ onSuccessAuth, handleChangeNumber }) {
    const [username, setUsername] = useState("");
    const [phoneNumber, setPhoneNumber] = useState("");
    const [showOtpSetdForm, setShowOtpSendForm] = useState(false);
    const [otp, setOtp] = useState("");
    const [loading, setLoading] = useState(false);

    const onCaptchaVerify = () => {
        if (!window.recaptchaVerifier) {
            window.recaptchaVerifier = new RecaptchaVerifier(
                auth,
                "sign-in-button",
                {
                    size: "invisible",
                    callback: (response) => {
                        // onSignInSubmit();
                    },
                    "expired-callback": () => {
                        setLoading(false);
                        delete window.recaptchaVerifier
                    },
                }
            );
        }
    };

    const onSignInSubmit = (event) => {
        event.preventDefault();
        if (phoneNumber.length !== 15) {
            toast.error("Заполните номер полностью");
            return;
        }
        if (!username) {
            toast.error("Введите имя");
            return;
        }
        setLoading(true);
        onCaptchaVerify();
        const appVerifier = window.recaptchaVerifier;
        const pn = "+" + phoneNumber.replace(/\D+/g, "");
        signInWithPhoneNumber(auth, pn, appVerifier)
            .then((res) => {
                if (res) {
                    window.confirmationResult = res;
                    setShowOtpSendForm(true);
                    toast.success(`Код подтверждения отправлен на номер ${pn}`);
                }
            })
            .catch((err) => toast.error(String(err)))
            .finally(() => setLoading(false));
    };

    const otpVerify = () => {
        if (window.confirmationResult) {
            setLoading(true);
            window.confirmationResult
                .confirm(otp)
                .then(async (res) => {
                    onSuccessAuth(username);
                })
                .catch((err) => toast.error("Неверный код"))
                .finally(() => setLoading(false));
        } else
            toast.error(
                "Не удалось обработать, поробуй авторизоваться еще раз"
            );
    };

    const onChangeNumber = () => {
        handleChangeNumber();
    };

    return (
        <div className={classes.form}>
            {!showOtpSetdForm ? (
                <>
                    <div>
                        <img
                            className={classes.icon}
                            src={usernameIcon}
                            alt="Username icon"
                            htmlFor="user-name"
                        ></img>
                        <input
                            value={username}
                            onChange={(e) => setUsername(e.target.value)}
                            className={classes.input}
                            id="user-name"
                            name="user-name"
                            autoComplete="on"
                            placeholder="Имя"
                        ></input>
                    </div>

                    <div>
                        <PhoneIcon
                            className={`${classes.icon} ${classes.phone_icon}`}
                        />
                        <MaskedInput
                            className={classes.input}
                            id="user-phone-number"
                            name="user-phone-number"
                            autoComplete="off"
                            placeholder="Номер телефона"
                            mask={"996(000)000-000"}
                            onAccept={(value, mask) => {
                                mask.updateValue();
                                setPhoneNumber(value);
                            }}
                        />
                    </div>
                </>
            ) : (
                <div>
                    <OtpCodeIcon
                        className={classes.icon}
                        alt="Username icon"
                        htmlFor="user-name"
                    />
                    <input
                        value={otp}
                        onChange={(e) => setOtp(e.target.value)}
                        className={classes.input}
                        id="user-name"
                        name="user-name"
                        autoComplete="on"
                        placeholder="Код подтверждения"
                    ></input>
                </div>
            )}
            {!showOtpSetdForm ? (
                <button
                    id="sign-in-button"
                    onClick={onSignInSubmit}
                    className={classes.loginBtn}
                >
                    {loading ? "Загрузка..." : "Отправить код"}
                </button>
            ) : (
                <>
                    <button onClick={otpVerify} className={classes.loginBtn}>
                        {loading ? "Загрузка..." : "Подтвердить"}
                    </button>
                    <button
                        onClick={onChangeNumber}
                        className={classes.textBtn}
                    >
                        Отправить на другой номер
                    </button>
                </>
            )}
        </div>
    );
}

export default AuthForm;
