// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCF3EOzNSX9ky38DWyHgNf2S3Avfz3BWoM",
  authDomain: "testauth-163d0.firebaseapp.com",
  projectId: "testauth-163d0",
  storageBucket: "testauth-163d0.appspot.com",
  messagingSenderId: "277918813456",
  appId: "1:277918813456:web:dc1a9245df2c0b5737a995"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app)