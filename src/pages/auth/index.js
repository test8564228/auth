import AuthForm from "../../components/AuthForm";
/**
 * @param onSuccessAuth () => void
 * 
 * @example
 * 
 * const onSuccessAuth = useCallback(
        (name) => {
            setIsAuth(true);
            toast.success(`Вы успешно прошли авторизацию`);
            setUser(name);
        },
        [setIsAuth, setUser]
    );
 */
function AuthPage({ onSuccessAuth, handleChangeNumber }) {
    return ( 
        <AuthForm onSuccessAuth={onSuccessAuth} handleChangeNumber={handleChangeNumber}/>
     );
}

export default AuthPage;