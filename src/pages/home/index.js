function HomePage({ name }) {
    return (
        <>
            <h2>Добро пожаловать {name}! </h2>
        </>
    );
}

export default HomePage;
