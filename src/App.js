import { useCallback, useEffect, useState } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import toast, { Toaster } from "react-hot-toast";

import AuthPage from "./pages/auth";
import HomePage from "./pages/home";
function App() {
    const [isAuth, setIsAuth] = useState(false);
    const [user, setUser] = useState("");
    const navigate = useNavigate();

    useEffect(() => {
        if (isAuth) return navigate("home");
        else navigate("auth");
    }, [isAuth]);

    const onSuccessAuth = useCallback(
        (name) => {
            setIsAuth(true);
            toast.success(`Вы успешно прошли авторизацию`);
            setUser(name);
        },
        [setIsAuth, setUser]
    );

    const handleChangeNumber = useCallback(
        (name) => {
            setIsAuth(false);
            setUser("");
        },
        [setIsAuth, setUser]
    );
    return (
        <div className="main">
            <Toaster />
            <Routes>
                <Route
                    path="auth"
                    element={
                        <AuthPage
                            handleChangeNumber={handleChangeNumber}
                            onSuccessAuth={onSuccessAuth}
                        />
                    }
                />
                <Route path="home" element={<HomePage name={user} />} />
            </Routes>
        </div>
    );
}

export default App;
